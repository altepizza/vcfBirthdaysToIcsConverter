import argparse
parser = argparse.ArgumentParser(description='Create a birthday calendar')
parser.add_argument('--source', dest='sourcePath', action='store', required = True,
                   help='define source .vcf file which holds at lest one valid contact')
parser.add_argument('--ics', dest='targetPath', action='store', required = True,
                   help='define output .ics file. File will be overwritten.')

args = parser.parse_args()
inputVcfPath = args.sourcePath
outIcsPath = args.targetPath

import vobject
from datetime import datetime
from icalendar import Calendar, Event

#not necessary anymore
import pytz
tz = pytz.timezone('Europe/Berlin')

inputVcf = open(inputVcfPath, 'r')
vcard = vobject.readComponents(inputVcf)

cal = Calendar()
from dateutil.rrule import *
for a in vcard:
    if a.getChildValue('bday') is not None:
        print (a.getChildValue('fn'))
        print (a.getChildValue('bday'))
        event = Event()
        event.add('summary', 'Geburtstag: ' + a.getChildValue('fn'))
        evTime = None
        try:
            evTime = datetime.strptime(a.getChildValue('bday'), '%Y-%m-%d')
        except:
            evTime = datetime.strptime(a.getChildValue('bday'), '%Y%m%d')
        #TODO localize still needed? Are birthdays location based? I don't no
        evTime = tz.localize(evTime)
        evTime = evTime.date()
        event.add('dtstart', evTime)
        event.add('rrule', {'freq': 'yearly'})
        cal.add_component(event)
if not outIcsPath.endswith(".ics"):
    outIcsPath = outIcsPath + ".ics"
outIcs = open(outIcsPath, 'wb')
outIcs.write(cal.to_ical())
outIcs.close()
